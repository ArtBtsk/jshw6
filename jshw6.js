function createNewUser() {
  let firstName = prompt("Введіть ваше ім'я:");
  let lastName = prompt("Введіть ваше прізвище:");
  let birthdate = prompt(
    "Введіть вашу дату народження (у форматі dd.mm.yyyy):"
  );

  let newUser = {
    firstName: firstName,
    lastName: lastName,
    birthdate: birthdate,
    getAge: function () {
      let currentDate = new Date();
      let birthdateParts = this.birthdate.split(".");
      let birthdateObj = new Date(
        birthdateParts[2],
        birthdateParts[1] - 1,
        birthdateParts[0]
      );
      let age = currentDate.getFullYear() - birthdateObj.getFullYear();

      // Перевірка, чи день народження вже пройшов у поточному році
      if (
        currentDate.getMonth() < birthdateObj.getMonth() ||
        (currentDate.getMonth() === birthdateObj.getMonth() &&
          currentDate.getDate() < birthdateObj.getDate())
      ) {
        age--;
      }

      return age;
    },
    getPassword: function () {
      let password =
        this.firstName.charAt(0).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthdate.split(".").reverse().join("");
      return password;
    },
    getLogin: function () {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    },
  };

  return newUser;
}

let user = createNewUser();
console.log("User:", user);
console.log("Age:", user.getAge());
console.log("Password:", user.getPassword());
